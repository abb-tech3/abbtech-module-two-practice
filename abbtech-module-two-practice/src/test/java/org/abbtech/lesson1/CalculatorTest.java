package org.abbtech.lesson1;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void init(){
        calculator=new Calculator();
    }

    @Test
    void classNotNullTest(){
        Assertions.assertNotNull(calculator);
    }


    @Test
    void positiveAdditionTest(){
        Assertions.assertEquals(30, calculator.addition(10, 20));
    }

    @Test
    void negativeAdditionTest(){
        Assertions.assertEquals(-5,calculator.addition(-2,-3));
    }

    @Test
    void  positiveSubtractionTest(){
        Assertions.assertEquals(5, calculator.subtraction(10, 5));
    }

    @Test
    void  negativeSubtractionTest(){
        Assertions.assertEquals(-5,calculator.subtraction(-15,-10));
    }

    @Test
    void  positiveMultiplicationTest(){
        Assertions.assertEquals(42, calculator.multiplication(6, 7));
    }
    @Test
    void  zeroMultiplicationTest(){
        Assertions.assertEquals(0, calculator.multiplication(99, 0));
    }
    @Test
    void  negativeMultiplicationTest(){
        Assertions.assertEquals(-42, calculator.multiplication(-6, 7));
    }


    @Test
    void  divisionTest(){
        Assertions.assertEquals(4.0, calculator.division(64, 16));
    }

    @Test
    void  decimalDivisionTest(){
        Assertions.assertEquals(30.0/7.0, calculator.division(30, 7),0.0001);
    }

    @Test
    void divisionZeroTest() {
        Assertions.assertThrows(IllegalArgumentException.class,()->{
            calculator.division(10,0);
        });

    }
}
