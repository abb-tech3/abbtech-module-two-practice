package org.abbtech.lesson2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImpTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImp userServiceImp;

    @Test
    void isUserActive() {
        when(userRepository.findByUsername(anyString())).thenReturn(new User("Elvin", true));
        boolean actual = userServiceImp.isUserActive("Murad");
        Assertions.assertEquals(true, actual);
    }

    @Test
    void deleteUser_success() {
        User user=new User("Elvin", true);
        when(userRepository.findUserId(anyLong())).thenReturn(user);
        Assertions.assertDoesNotThrow(() -> {
            userServiceImp.deleteUser(2L);
        });
    }

    @Test
    void deleteUser_unsuccess() {
        User user=new User("Elvin", true);
        when(userRepository.findUserId(anyLong())).thenReturn(null);
        Assertions.assertThrows(Exception.class,
                ()->{userServiceImp.deleteUser(2L);
        });
    }

    @Test
    void getUser_success() throws Exception {
        User user=new User("Elvin", true);
        when(userRepository.findUserId(anyLong())).thenReturn(user);
        Assertions.assertDoesNotThrow(() -> {
            userServiceImp.getUser(2L);
        });
        Assertions.assertSame(user,userServiceImp.getUser(2L));
    }

    @Test
    void getUser_unsuccess()  {
        when(userRepository.findUserId(anyLong())).thenReturn(null);
        Assertions.assertThrows(Exception.class,() -> {
            userServiceImp.getUser(2L);
        });
    }

}
