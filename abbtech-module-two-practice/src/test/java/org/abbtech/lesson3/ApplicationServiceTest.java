package org.abbtech.lesson3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceTest {

    @Mock
    private CalculatorService calculatorService;

    @InjectMocks
    private ApplicationService applicationService;



    @Test
    void multiplySuccessfulTest(){
        Mockito.when(calculatorService.multiply(3,5)).thenReturn(15);
        Assertions.assertEquals(15,applicationService.multiply(3,5));
    }

    @Test
    void multiplyExceptionTest(){
        Assertions.assertThrows(ArithmeticException.class,()->applicationService.multiply(5,7));
    }

    @Test
    void divisionSuccessfulTest(){
        Mockito.when(calculatorService.division(16,8)).thenReturn(2.0);
        Assertions.assertEquals(2.0,applicationService.division(16,8));
    }

    @Test
    void divisionExceptionTest(){
        Assertions.assertThrows(ArithmeticException.class,()->applicationService.division(5,7));
    }

    @Test
    void additionSuccessfulTest(){
        Mockito.when(calculatorService.addition(3,5)).thenReturn(8);
        Assertions.assertEquals(8,applicationService.addition(3,5));
    }

    @Test
    void additionExceptionTest(){
        Assertions.assertThrows(ArithmeticException.class,()->applicationService.addition(-5,7));
    }

    @Test
    void subtractSuccessfulTest(){
        Mockito.when(calculatorService.subtract(9,6)).thenReturn(3);
        Assertions.assertEquals(3,applicationService.subtract(9,6));
    }

    @Test
    void subtractExceptionTest(){
        Mockito.when(calculatorService.subtract(9,5)).thenReturn(4);
        Assertions.assertThrows(ArithmeticException.class,()->applicationService.subtract(9,5));
    }
}
