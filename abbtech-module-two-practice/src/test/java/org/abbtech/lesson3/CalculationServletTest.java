package org.abbtech.lesson3;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CalculationServletTest {

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;


    @Mock
    private ApplicationService mockApplicationService;

    @InjectMocks
    private CalculationServlet calculationServlet;

    @BeforeEach
    public void setUp() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(mockResponse.getWriter()).thenReturn(writer);
    }



    @Test
    void multiplyTest() throws ServletException, IOException {
        when(mockRequest.getHeader("x-calculation-method")).thenReturn("multiply");
        when(mockRequest.getParameter("a")).thenReturn("5");
        when(mockRequest.getParameter("b")).thenReturn("3");

        calculationServlet.doGet(mockRequest, mockResponse);

        verify(mockApplicationService).multiply(5, 3);

    }

    @Test
    void divisionTest() throws ServletException, IOException {
        when(mockRequest.getHeader("x-calculation-method")).thenReturn("division");
        when(mockRequest.getParameter("a")).thenReturn("10");
        when(mockRequest.getParameter("b")).thenReturn("2");


        calculationServlet.doGet(mockRequest, mockResponse);

        verify(mockApplicationService).division(10, 2);
    }

    @Test
    void subtractTest() throws ServletException, IOException {
        when(mockRequest.getHeader("x-calculation-method")).thenReturn("subtract");
        when(mockRequest.getParameter("a")).thenReturn("8");
        when(mockRequest.getParameter("b")).thenReturn("2");


        calculationServlet.doGet(mockRequest, mockResponse);
        verify(mockApplicationService).subtract(8, 2);
    }

    @Test
    void additionTest() throws ServletException, IOException {
        when(mockRequest.getHeader("x-calculation-method")).thenReturn("addition");
        when(mockRequest.getParameter("a")).thenReturn("8");
        when(mockRequest.getParameter("b")).thenReturn("3");


        calculationServlet.doGet(mockRequest, mockResponse);

        verify(mockApplicationService).addition(8, 3);
    }

    @Test
    void calculationMethodTest() throws ServletException, IOException {
        when(mockRequest.getRequestURI()).thenReturn("/calculator/addition");
        when(mockRequest.getParameter("a")).thenReturn("8");
        when(mockRequest.getParameter("b")).thenReturn("3");


        calculationServlet.doGet(mockRequest, mockResponse);

        verify(mockApplicationService).addition(8, 3);
        verify(mockResponse).setContentType("application/json");

    }

    @Test
    void nullParameterTest() throws IOException, ServletException {
        when(mockRequest.getRequestURI()).thenReturn("/calculator/addition");
        when(mockRequest.getParameter("a")).thenReturn("3");
        when(mockRequest.getParameter("b")).thenReturn(null);

        calculationServlet.doGet(mockRequest,mockResponse);

        verify(mockResponse).setStatus(HttpServletResponse.SC_BAD_REQUEST);

    }








}

