package org.abbtech.lesson3;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/multiply", "/calculator/addition", "/calculator/division", "/calculator/subtract"})
public class CalculationServlet extends HttpServlet {

    private ApplicationService applicationService;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        String calculationMethod = req.getHeader("x-calculation-method");
        if (calculationMethod == null) {
            calculationMethod = req.getRequestURI().substring(12);
        }
        int a = 0;
        int b = 0;
        try {
            a = Integer.parseInt(req.getParameter("a"));
            b = Integer.parseInt(req.getParameter("b"));
        } catch (NumberFormatException numberFormatException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    "exception": "USER MUST SENT 2 VALUE a AND b":""" + numberFormatException.getMessage() + """
                    """);
            return;
        }

        double result = 0;

        try {
            switch (calculationMethod.toLowerCase()) {
                case "multiply": {
                    result = applicationService.multiply(a, b);
                    break;
                }
                case "division": {
                    result = applicationService.division(a, b);
                    break;
                }
                case "addition": {
                    result = applicationService.addition(a, b);
                    break;
                }
                case "subtract": {
                    result = applicationService.subtract(a, b);
                    break;
                }
            }
            writer.write("""
                    {
                            "result":""" + result + """
                                
                          }
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS":""" + arithmeticException.getMessage() + """
                    }                    
                    """);
        }


    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int lineLine;
        while ((lineLine = bufferedReader.read()) != -1) {
            char chr = (char) lineLine;
            writer.write(chr);
        }
        writer.close();

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}

