package org.abbtech.lesson3;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "CalculationMainServlet", urlPatterns = "/calculator")
public class CalculationMainServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("text/html");
        writer.write("""
                     <!DOCTYPE html>
                     <html lang="en">
                     <head>
                         <meta charset="UTF-8">
                         <meta name="viewport" content="width=device-width, initial-scale=1.0">
                         <title>Routing Example</title>
                     </head>
                     <body>
                         <h1>Calculator</h1>
                         <p>Select which math operation you want to perform:</p>
                         <ul>
                             <li><a href="http://localhost:8080/calculator/multiply">Multiply</a></li>
                             <li><a href="http://localhost:8080/calculator/division">Division</a></li>
                             <li><a href="http://localhost:8080/calculator/add">Addition</a></li>
                             <li><a href="http://localhost:8080/calculator/subtract">Subtraction</a></li>
                         </ul>
                     </body>
                     </html>
                """);


    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int lineLine;
        while ((lineLine = bufferedReader.read()) != -1) {
            char chr = (char) lineLine;
            writer.write(chr);
        }
        writer.close();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
