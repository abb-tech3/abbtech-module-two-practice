package org.abbtech.lesson2;

public class User {

    private static int userId=0;
    private String username;
    private boolean usrStatus;

    public User(String username, boolean usrStatus) {
        userId++;
        this.username = username;
        this.usrStatus = usrStatus;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public boolean isActive() {
        return usrStatus;
    }
}
