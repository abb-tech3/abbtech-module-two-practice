package org.abbtech.lesson2;

public class UserServiceImp implements UserService {
    public final UserRepository userRepository;
    public UserServiceImp(UserRepository userRepository){
        this.userRepository=userRepository;
    }

    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(Long userId) throws Exception {
        User user = userRepository.findUserId(userId);
        if(user==null){
            throw new Exception();
        }
    }

    public User getUser(Long userId) throws Exception {
        User user = userRepository.findUserId(userId);
        if(user==null){
            throw new Exception();
        }
        return user;
    }
}
